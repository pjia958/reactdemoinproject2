import React, {Component} from 'react';

export default class MyLikeButton extends React.Component{
    constructor(){
        super()
        this.state = {
            Like : true
        }
    }

    handleClick(){
        console.log(this.state.Like)
        this.setState({
            Like: !this.state.Like
        })
        this.render()
    }

    render(){
        return(
        <button type="button" onClick={()=>this.handleClick()}>{this.state.Like ? 'Like' : 'Liked'}</button>
        )
    }
}