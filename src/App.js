import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MyLikeButton from './myLikeButton';

class Button extends Component {
  constructor(props){
    super(props)
    this.state = {
      themeColor : ''
    }
  }

  // handleClick(color){
  //   // console.log(color)
  //   this.setState({
  //     themeColor : color
  //   })
  // }
  

  render(){
    var themeColor = this.props.themeColor
    console.log(this.props)

    return(
    <div>
      <button onClick={()=>this.props.handleClick('red')} style={{color : themeColor}}>Red</button>

      <button onClick={()=>this.props.handleClick('green')} style={{color : themeColor}}>Green</button>
    </div>
  )}
}

class Title extends Component{
  constructor(){
    super()
    this.state = {
      themeColor : ''
    }
  }
  render(){
    const themeColor = this.props.themeColor
    return(
      <h2 style={{color:themeColor}}>This is a title</h2>
    )
  }
}

class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      themeColor : ''
    }
  }

  handleClick(color){
    console.log(color)
    this.setState({
      themeColor : color
    })
  }
  
  render() {
    return (
      <div>
        <h5>This is an App</h5>
        <Title themeColor={this.state.themeColor}></Title>
        <Button themeColor={this.state.themeColor} handleClick={(color)=>this.handleClick(color)}></Button>
        </div>
    )
  }
}

export default App;
